import React from 'react'
import PropTypes from 'prop-types'

function AddToCartModal({ header, closeButton, text, actions, setActive }) {
  return (
    <div
      className="modal-backdrop"
      onClick={() => {
        setActive(false)
      }}
    >
      <div
        className="modal add-to-cart-modal"
        onClick={event => {
          event.stopPropagation()
        }}
      >
        <h1 className="modal__header add-to-cart-modal__header">{header}</h1>
        {{ closeButton } && (
          <button
            className="modal__close-btn"
            onClick={() => {
              setActive(false)
            }}
          ></button>
        )}
        <p className="modal__body add-to-cart-modal__body">{text}</p>
        {actions}
      </div>
    </div>
  )
}

AddToCartModal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string.isRequired,
  actions: PropTypes.element.isRequired,
  setActive: PropTypes.func.isRequired,
}

AddToCartModal.defaultProps = {
  header: 'Adding to cart confirmation',
  closeButton: 'true',
}

export default AddToCartModal
