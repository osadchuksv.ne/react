import React, { useState, useEffect } from 'react'
import './App.scss'
import ActionButton from './components/ActionButton'
import Header from './components/Header'
import ProductList from './components/ProductsList'
import AddToCartModal from './components/AddToCartModal'

function App() {
  const [productList, setProductList] = useState([])
  const [cartItems, setCartItems] = useState([])
  const [favoriteItems, setFavoriteItems] = useState([])
  const [addToCartModalVisibility, setAddToCartModalVisibility] = useState(false)
  const [itemId, setItemId] = useState(null)
  const [currentItemName, setCurrentItemName] = useState(null)

  useEffect(() => {
    setCartItems(JSON.parse(localStorage.getItem('cartItems')) || [])
    setFavoriteItems(JSON.parse(localStorage.getItem('favoriteItems')) || [])
  }, [])

  useEffect(() => {
    try {
      const fetchData = async () => {
        const data = await fetch('/products.json').then(res => res.json())
        setProductList(data.products)
        console.log(data)
        localStorage.setItem('products', JSON.stringify(data.products))
      }
      fetchData()
    } catch (error) {
      console.error(error)
    }
  }, [])

  const addToCart = id => {
    if (cartItems.includes(id)) {
      return
    }
    const updatedCartList = [...cartItems, id]
    setCartItems(updatedCartList)

    localStorage.setItem('cartItems', JSON.stringify(updatedCartList))
  }

  const addToCartModalActionBtns = (
    <div className="modal__actions">
      <ActionButton
        name="Add to cart"
        className="add-to-cart-btn modal-btn"
        handleClick={() => {
          addToCart(itemId)
          setAddToCartModalVisibility(false)
          setItemId(null)
        }}
      />
      <ActionButton
        name="Cancel"
        className="cancel-btn modal-btn "
        handleClick={() => {
          setAddToCartModalVisibility(false)
          setItemId(null)
        }}
      />
    </div>
  )

  return (
    <>
      <Header
        favIconType="header__favorites-icon"
        cartItems={cartItems}
        favoriteItems={favoriteItems}
      />
      <ProductList
        productList={productList}
        setItemId={setItemId}
        handleBuyClick={() => {
          setAddToCartModalVisibility(true)
        }}
        setCurrentItemName={setCurrentItemName}
        favoriteItems={favoriteItems}
        setFavoriteItems={setFavoriteItems}
        cartItems={cartItems}
        setCartItems={setCartItems}
      />
      {addToCartModalVisibility && (
        <AddToCartModal
          header="Adding to cart confirmation"
          closeButton="true"
          text={`By clicking "Add to cart" button you will add ${currentItemName} to the cart. To buy chosen item you should go to the cart page and confirm purchase`}
          actions={addToCartModalActionBtns}
          active={addToCartModalVisibility}
          setActive={setAddToCartModalVisibility}
          itemId={itemId}
        />
      )}
    </>
  )
}

export default App
