import Button from './Button.js'
import { render, screen, fireEvent } from '@testing-library/react'

describe('Buton component tests', () => {
  test('should "Buy" button be rendered', () => {
    render(
      <Button
        background="green"
        name="Buy"
        className="product-card__buy-btn"
        onClick={() => {}}
        disabled={false}
      />,
    )

    const buyBtn = screen.getByText('Buy')
    expect(buyBtn).toBeInTheDocument()
  })

  test('should render button with correct name', () => {
    const { getByText } = render(<Button name="Click me" />)
    expect(getByText('Click me')).toBeInTheDocument()
  })

  test('should render button with correct background color', () => {
    const { container } = render(<Button name="Button" background="red" />)
    const button = container.querySelector('button')
    expect(button).toHaveStyle('background-color: red')
  })

  test('should render button with correct className', () => {
    const { container } = render(<Button name="Button" className="test-class" />)
    const button = container.querySelector('button')
    expect(button).toHaveClass('test-class')
  })

  test('should call onClick function when button is clicked', () => {
    const handleClick = jest.fn()
    const { getByText } = render(<Button name="Button" onClick={handleClick} />)
    fireEvent.click(getByText('Button'))
    expect(handleClick).toHaveBeenCalled()
  })

  test('should match the snapshot', () => {
    const { asFragment } = render(<Button name="Button" />)
    expect(asFragment()).toMatchSnapshot()
  })
})
