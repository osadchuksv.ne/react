import React from 'react'
import { Formik, Form } from 'formik'
import validationSchema from './validationSchema'
import Input from '../CustomInput'

const DeliveryDataForm = ({ handleFormSubmit }) => {
  const initialValues = {
    'First name': '',
    'Last name': '',
    Age: '',
    'Delivery address': '',
    'Phone number': '',
  }

  const onSubmit = (values, actions) => {
    console.log(values)
    handleFormSubmit()
  }

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
    >
      {({ isValid }) => {
        return (
          <Form className="delivery-form">
            <h2>Please fill in the delivery data</h2>
            <Input type="text" name="First name" placeholder="first name" />
            <Input type="text" name="Last name" placeholder="last name" />
            <Input type="text" name="Age" placeholder="age" />
            <Input type="text" name="Delivery address" placeholder="delivery address" />
            <Input type="text" name="Phone number" placeholder="phone number" />
            <button type="submit" disabled={!isValid} className="delivery-form__checkout-btn">
              Checkout
            </button>
          </Form>
        )
      }}
    </Formik>
  )
}

export default DeliveryDataForm
