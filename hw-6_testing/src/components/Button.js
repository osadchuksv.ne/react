import React from 'react'
import PropTypes from 'prop-types'

function Button({ background, name, className, onClick, disabled = false }) {
  return (
    <>
      <button
        disabled={disabled}
        className={className}
        style={{ backgroundColor: background }}
        onClick={onClick}
      >
        {name}
      </button>
    </>
  )
}

Button.propTypes = {
  background: PropTypes.string,
  name: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}
Button.defaultProps = {
  background: 'green',
}

export default Button
