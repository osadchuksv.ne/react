// import AddToCartModal from './AddToCartModal.js'
// import RemoveFromCartModal from './RemoveFromCartModal.js'
// import { render, screen, fireEvent } from '@testing-library/react'

import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import AddToCartModal from './AddToCartModal'

const mockStore = configureStore([])

describe('AddToCartModal component', () => {
  it('should render header and text correctly', () => {
    const headerText = 'Test Header'
    const bodyText = 'Test Body'
    const initialState = {
      products: {},
    }
    const store = mockStore(initialState)

    const { getByText } = render(
      <Provider store={store}>
        <AddToCartModal header={headerText} text={bodyText} />
      </Provider>,
    )

    expect(getByText(headerText)).toBeInTheDocument()
    expect(getByText(bodyText)).toBeInTheDocument()
  })

  it('should render Add to cart and Cancel buttons', () => {
    const initialState = {
      products: {},
    }
    const store = mockStore(initialState)

    const { getByText } = render(
      <Provider store={store}>
        <AddToCartModal />
      </Provider>,
    )

    expect(getByText('Add to cart')).toBeInTheDocument()
    expect(getByText('Cancel')).toBeInTheDocument()
  })

  test('should dispatch addToCart action on Add to cart button click', () => {
    const initialState = {
      products: {
        selectedItemId: 123,
      },
    }
    const store = mockStore(initialState)
    const { getByText } = render(
      <Provider store={store}>
        <AddToCartModal />
      </Provider>,
    )

    fireEvent.click(getByText('Add to cart'))

    const actions = store.getActions()
    expect(actions).toEqual([
      { type: 'products/addToCart', payload: 123 },
      { type: 'products/setSelectedItemId', payload: null },
      { type: 'products/setSelectedItemName', payload: null },
      { type: 'products/closeModal' },
    ])
  })

  test('should dispatch setSelectedItemId, setSelectedItemName, and closeModal actions on Cancel button click', () => {
    const initialState = {
      products: {
        selectedItemId: 123,
      },
    }
    const store = mockStore(initialState)
    const { getByText } = render(
      <Provider store={store}>
        <AddToCartModal />
      </Provider>,
    )

    fireEvent.click(getByText('Cancel'))

    const actions = store.getActions()
    expect(actions).toEqual([
      { type: 'products/setSelectedItemId', payload: null },
      { type: 'products/setSelectedItemName', payload: null },
      { type: 'products/closeModal' },
    ])
  })

  it('should dispatch setSelectedItemId, setSelectedItemName, and closeModal actions on modal backdrop click', () => {
    const initialState = {
      products: {
        selectedItemId: 123,
      },
    }
    const store = mockStore(initialState)
    const { container } = render(
      <Provider store={store}>
        <AddToCartModal />
      </Provider>,
    )

    fireEvent.click(container.querySelector('.modal-backdrop'))

    const actions = store.getActions()
    expect(actions).toEqual([
      { type: 'products/setSelectedItemId', payload: null },
      { type: 'products/setSelectedItemName', payload: null },
      { type: 'products/closeModal' },
    ])
  })
})

// describe('common modals tests', () => {
//   test('should AddToCartModal be rendered with buttons', () => {
//     const addToCartBtn = screen.getByText('button', { name: 'Add to cart' })
//     console.log(addToCartBtn)
//     const cancelBtn = screen.getByRole('button', { name: 'Cancel' })
//     render(
//       <AddToCartModal
//         header="Add to cart modal test header"
//         closeButton={true}
//         text="Add to cart modal test text"
//       />,
//     )

//     expect(addToCartBtn).toBeInTheDocument()
//     expect(cancelBtn).toBeInTheDocument()
//     // const handleBuyBtnClick = jest.fn()
//     // fireEvent.click(buyBtn)
//   })

// test('should modal close on close icon click', () => {}),
// test('should modal close on backdrop click', () => {}),
// test('should AddToCartModal contain "Add to cart" button', () => {}),
// test('should AddToCartModal contain "Cancel" button', () => {}),
// test('should RemoveFromCartModal contain "Remove" button', () => {}),
// test('should RemoveFromCartModal contain "Cancel" button', () => {})
// })

// describe('Modal componet', () => {
//     test('Render modal component with buttons', () => {
//         const handleClose = jest.fn()
//         render(
//             <Modal isOpen={true} onClose={handleClose}>
//                 <div>Привіт, як справи?</div>
//             </Modal>
//         )

//         const modalContent = screen.getByText('Привіт, як справи?')
//         expect(modalContent).toBeInTheDocument();

//         const closeButton = screen.getByText('Close');
//         expect(closeButton).toBeInTheDocument();

//         const openButton = screen.getByText('Open Modal');
//         expect(openButton).toBeInTheDocument();

//         fireEvent.click(closeButton);
//         expect(handleClose).toHaveBeenCalledTimes(1);

//         fireEvent.click(openButton);
//         const reopenModalContent = screen.getByText('Привіт, як справи?');
//         expect(reopenModalContent).toBeInTheDocument();

//     })

//     test('Dont render with isOpen=false', () => {
//         render(
//             <Modal isOpen={false}>
//             <div>Привіт, як справи?</div>
//         </Modal>
//         )
//         const modalContent = screen.queryByText('Привіт, як справи?')
//         expect(modalContent).toBeNull();
//     })
// })
