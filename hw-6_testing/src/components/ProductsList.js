import React, { useEffect, useContext } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchProducts } from '../redux/slices/productsSlice'
import Context from '../context'
import Card from './Card'

function ProductList() {
  const dispatch = useDispatch()
  const productsList = useSelector(state => state.products.productsList)

  const { currentMode } = useContext(Context)

  useEffect(() => {
    dispatch(fetchProducts())
  }, [dispatch])

  return (
    <div className={currentMode === 'card' ? 'cards-wrapper' : 'cards-wrapper table-mode'}>
      {productsList?.map(item => (
        <Card
          key={item.id}
          id={item.id}
          name={item.name}
          price={item.price}
          imgSrc={item.imgSrc}
          color={item.color}
        />
      ))}
    </div>
  )
}

export default ProductList
