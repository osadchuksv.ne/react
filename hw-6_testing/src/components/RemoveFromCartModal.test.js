import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import RemoveFromCartModal from './RemoveFromCartModal'

const mockStore = configureStore([])

describe('RemoveFromCartModal component', () => {
  test('should render header and text correctly', () => {
    const headerText = 'Test Header'
    const bodyText = 'Test Body'
    const initialState = {
      products: {},
    }
    const store = mockStore(initialState)

    const { getByText } = render(
      <Provider store={store}>
        <RemoveFromCartModal header={headerText} text={bodyText} />
      </Provider>,
    )

    expect(getByText(headerText)).toBeInTheDocument()
    expect(getByText(bodyText)).toBeInTheDocument()
  })

  test('should render Remove from cart and Cancel buttons', () => {
    const initialState = {
      products: {},
    }
    const store = mockStore(initialState)

    const { getByText } = render(
      <Provider store={store}>
        <RemoveFromCartModal />
      </Provider>,
    )

    expect(getByText('Remove from cart')).toBeInTheDocument()
    expect(getByText('Cancel')).toBeInTheDocument()
  })

  test('should dispatch setSelectedItemId, setSelectedItemName, and closeModal actions on Cancel button click', () => {
    const initialState = {
      products: {
        selectedItemId: 123,
      },
    }
    const store = mockStore(initialState)
    const { getByText } = render(
      <Provider store={store}>
        <RemoveFromCartModal />
      </Provider>,
    )

    fireEvent.click(getByText('Cancel'))

    const actions = store.getActions()
    expect(actions).toEqual([
      { type: 'products/setSelectedItemId', payload: null },
      { type: 'products/setSelectedItemName', payload: null },
      { type: 'products/closeModal' },
    ])
  })
})
