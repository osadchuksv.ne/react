import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import Card from './Card'

const mockStore = configureStore([])

describe('Card component', () => {
  test('should match the snapshot', () => {
    const store = mockStore({
      products: {
        favoriteItems: [],
        cartItems: [],
        isModalOpen: false,
        selectedItemId: null,
        selectedItemName: null,
      },
    })
    const { asFragment } = render(
      <Provider store={store}>
        <Card
          id={1}
          name="iPhone 13 128Gb"
          price={31999}
          imgSrc="https://content1.rozetka.com.ua/goods/images/big/221214139.jpg"
          color="Midnight"
        />
      </Provider>,
    )
    expect(asFragment()).toMatchSnapshot()
  })

  test('should render card with correct name, price, and color', () => {
    const store = mockStore({
      products: {
        favoriteItems: [],
        cartItems: [],
        isModalOpen: false,
        selectedItemId: null,
        selectedItemName: null,
      },
    })
    const { getByText } = render(
      <Provider store={store}>
        <Card
          id={1}
          name="iPhone 13 128Gb"
          price={31999}
          imgSrc="https://content1.rozetka.com.ua/goods/images/big/221214139.jpg"
          color="Midnight"
        />
      </Provider>,
    )
    expect(getByText('iPhone 13 128Gb')).toBeInTheDocument()
    expect(getByText('31999')).toBeInTheDocument()
    expect(getByText(/Midnight/)).toBeInTheDocument()
  })

  test('should display addToCart modal when isModalOpen is true', () => {
    const store = mockStore({
      products: {
        favoriteItems: [],
        cartItems: [],
        isModalOpen: true,
        selectedItemId: 1,
        selectedItemName: 'iPhone 13 128Gb',
      },
    })
    const { getByText } = render(
      <Provider store={store}>
        <Card
          id={1}
          name="iPhone 13 128Gb"
          price={31999}
          imgSrc="https://content1.rozetka.com.ua/goods/images/big/221214139.jpg"
          color="Midnight"
          isModalOpen={true}
          selectedItemName="iPhone 13 128Gb"
        />
      </Provider>,
    )
    expect(getByText('Adding to cart confirmation')).toBeInTheDocument()
  })
})
