import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { clearCart } from '../redux/slices/productsSlice'
import CartItem from '../components/CartItem'
import Button from '../components/Button'
import DeliveryDataForm from '../components/Forms/DeliveryDataForm'

const CartPage = () => {
  const [purchasedList, setPurchasedList] = useState([])

  const productsList = useSelector(state => state.products.productsList)
  const cartItems = useSelector(state => state.products.cartItems)

  const dispatch = useDispatch()

  /* При додаванні в корзину товару, створюється об'єкт з id та counter (кількістю товару в корзині).Щоб відрендерити список доданих товарів, нам потрібні ін */
  useEffect(() => {
    let newPurchasedList = []
    // фільтруємо список товарів...
    const filteredProductList = productsList.filter(elem => {
      // залишаючи товари, id яких співпадають з id товарів у корзині
      return cartItems.find(item => {
        return item.id === elem.id
      })
    })
    // оголошуємо змінну у яку записуватимемо загальну суму корзини
    let priceSum = 0
    // для кожного товара у відфільтрованому списку
    filteredProductList.forEach(item => {
      // витягуємо кількість товарів у корзині з властивості counter об'єкта товару
      const counter = cartItems.find(elem => {
        return elem.id === item.id
      }).counter
      // обраховуємо суму товару, виходячи з його кількості у корзині
      const elemPrice = item.price * counter
      priceSum += elemPrice

      newPurchasedList.push({ product: item.name, quantity: counter, price: elemPrice })
    })
    setPurchasedList(newPurchasedList)
    setTotalCartSum(priceSum)
  }, [cartItems])

  const [totalCartSum, setTotalCartSum] = useState(0)

  const handleFormSubmit = () => {
    console.log('Purchased products:')
    purchasedList.forEach(elem => {
      console.log(
        `${elem.product}, quantity: ${elem.quantity}, total price: ${elem.price} UAH`,
      )
    })
    dispatch(clearCart())
  }

  return (
    <section className="cart-page container">
      {cartItems.length > 0 ? (
        <h1 className="page-title">Products in cart</h1>
      ) : (
        <h1 className="page-title">No items have been added to cart yet</h1>
      )}
      <div className="cart-page__content">
        {productsList
          .filter(elem => cartItems.some(item => item.id === elem.id))
          .map(item => (
            <CartItem
              key={item.id}
              id={item.id}
              name={item.name}
              price={item.price}
              imgSrc={item.imgSrc}
              color={item.color}
              setTotalCartSum={setTotalCartSum}
            />
          ))}
        {cartItems.length > 0 && (
          <div className="cart-page__total-next-step-div">
            <p className="cart-page__total-sum">
              Total: <span className="cart-page__total-sum_number">{totalCartSum}</span>
              <span> ₴</span>
            </p>
          </div>
        )}
      </div>
      {cartItems.length > 0 && <DeliveryDataForm handleFormSubmit={handleFormSubmit} />}
    </section>
  )
}

export { CartPage }
