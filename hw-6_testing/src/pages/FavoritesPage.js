import React from 'react'
import { useSelector } from 'react-redux'
import Card from '../components/Card'

const FavoritesPage = () => {

  const productsList = useSelector((state) => state.products.productsList);
  const favoriteItems = useSelector((state) => state.products.favoriteItems);


  return (
    <section className="favorites-page  container">
      {favoriteItems.length > 0 ? <h1 className='page-title'>Favorite items</h1> : <h1 className='page-title'>No items have been added to favorites yet</h1>}
        <div className="cards-wrapper">
          {productsList
            .filter(elem => favoriteItems.some(item => item === elem.id))
            .map(item => (
              <Card
                key={item.id}
                id={item.id}
                name={item.name}
                price={item.price}
                imgSrc={item.imgSrc}
                color={item.color}
              />
            ))}
        </div>
    </section>
  )
}

export { FavoritesPage }
