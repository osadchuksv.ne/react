import reducer, {
  setSelectedItemId,
  setSelectedItemName,
  addToCart,
  removeFromCart,
  clearCart,
  toggleFavorite,
  openModal,
  closeModal,
  fetchProducts,
} from './productsSlice.js'

describe('products reducer', () => {
  test('should handle fetchProducts pending', () => {
    const initialState = {
      status: 'idle',
    }
    const action = fetchProducts.pending()
    const nextState = reducer(initialState, action)
    expect(nextState.status).toEqual('loading')
  })

  test('should handle fetchProducts fulfilled', () => {
    const initialState = {
      productsList: [],
      status: 'idle',
    }
    const productsData = [
      {
        id: 123,
        name: 'test name',
        price: 31999,
        imgSrc: '#',
        color: 'Midnight',
      },
    ]
    const action = fetchProducts.fulfilled({ products: productsData })
    const nextState = reducer(initialState, action)
    expect(nextState.status).toEqual('resolved')
    expect(nextState.productsList).toEqual(productsData)
  })

  test('should handle fetchProducts rejected', () => {
    const initialState = {
      status: 'idle',
    }
    const error = new Error('Fetch failed')
    const action = fetchProducts.rejected(error)
    const nextState = reducer(initialState, action)
    expect(nextState.status).toEqual('rejected')
  })

  test('should handle addToCart', () => {
    const initialState = {
      cartItems: [],
    }
    const itemIdToAdd = 123
    const action = addToCart(itemIdToAdd)
    const nextState = reducer(initialState, action)

    expect(nextState.cartItems).toHaveLength(initialState.cartItems.length + 1)
    expect(nextState.cartItems).toEqual([{ counter: 1, id: 123 }])
  })

  test('should handle adding to favorites with toggleFavorites', () => {
    const initialState = {
      favoriteItems: [],
    }
    const itemId = 123
    const action = toggleFavorite(itemId)
    const nextState = reducer(initialState, action)
    expect(nextState.favoriteItems).toHaveLength(initialState.favoriteItems.length + 1)
    expect(nextState.favoriteItems).toEqual([123])
  })

  test('should handle removing from favorites with toggleFavorites', () => {
    const initialState = {
      favoriteItems: [123, 124],
    }
    const itemId = 123
    const action = toggleFavorite(itemId)
    const nextState = reducer(initialState, action)

    expect(nextState.favoriteItems).toHaveLength(initialState.favoriteItems.length - 1)
    expect(nextState.favoriteItems).toEqual([124])
  })

  test('should handle setSelectedItemId', () => {
    const initialState = {
      selectedItemId: null,
    }
    const selectedItemId = 123
    const action = setSelectedItemId(selectedItemId)
    const nextState = reducer(initialState, action)

    expect(nextState.selectedItemId).toBe(123)
  })

  test('should handle setSelectedItemName', () => {
    const initialState = {
      selectedItemName: null,
    }
    const selectedItemName = 'Iphone 14 Pro'
    const action = setSelectedItemName(selectedItemName)
    const nextState = reducer(initialState, action)

    expect(nextState.selectedItemName).toBe('Iphone 14 Pro')
  })

  test('should handle openModal', () => {
    const initialState = {
      isModalOpen: false,
    }

    const action = openModal()
    const nextState = reducer(initialState, action)

    expect(nextState.isModalOpen).toBe(true)
  })

  test('should handle closeModal', () => {
    const initialState = {
      isModalOpen: true,
    }

    const action = closeModal()
    const nextState = reducer(initialState, action)

    expect(nextState.isModalOpen).toBe(false)
  })

  test('should handle removeFromCart', () => {
    const initialState = {
      cartItems: [
        { id: 1, counter: 1 },
        { id: 2, counter: 1 },
      ],
    }
    const itemId = 1
    const action = removeFromCart(itemId)
    const nextState = reducer(initialState, action)
    expect(nextState.cartItems).toHaveLength(initialState.cartItems.length - 1)
    expect(nextState.cartItems).toEqual([{ id: 2, counter: 1 }])
  })

  test('should handle clearCart', () => {
    const initialState = {
      cartItems: [
        { id: 1, name: 'iPhone 13 128Gb', price: 31999 },
        { id: 2, name: 'iPhone 14 128Gb', price: 42499 },
      ],
    }
    const action = clearCart()
    const nextState = reducer(initialState, action)

    expect(nextState.cartItems).toEqual([])
  })
})
