import { configureStore } from "@reduxjs/toolkit";
import productsReducer from "./slices/productsSlice"
 /*експорт по дефолту тому будь-яке імя*/

const store = configureStore({
    reducer: {
        products: productsReducer
    }
    
})

export default store