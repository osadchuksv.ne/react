import Context from './context'
import { useEffect, useState } from 'react'

const ViewModeProvider = ({ children }) => {
  const [currentMode, setCurrentMode] = useState('')

  useEffect(() => {
    setCurrentMode(localStorage.getItem('viewMode') || 'card')
  }, [])

  const handleToggleClick = () => {
    setCurrentMode(prev => {
      const newMode = prev === 'table' ? 'card' : 'table'
      console.log(newMode)
      localStorage.setItem('viewMode', newMode)
      return newMode
    })
  }

  console.log(currentMode)

  return (
    <Context.Provider value={{ currentMode, handleToggleClick }}>{children}</Context.Provider>
  )
}

export default ViewModeProvider
