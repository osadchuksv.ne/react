import React from 'react'
import { Outlet } from 'react-router-dom'

import Header from './Header'

const Layout = ({ cartItems, favoriteItems }) => {
  return (
    <>
      {/* <div className="container"> */}
        <Header cartItems={cartItems} favoriteItems={favoriteItems} />
        <Outlet className="content" />
      {/* </div> */}
    </>
  )
}

export { Layout }
