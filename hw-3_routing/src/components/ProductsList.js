import React from 'react'
import Card from './Card'
import PropTypes from 'prop-types'

function ProductList({
  productList,
  handleBuyClick,
  setItemId,
  setCurrentItemName,
  favoriteItems,
  setFavoriteItems,
  cartItems
}) {
  return (
    <div className="cards-wrapper">
      {productList?.map(item => (
        <Card
          key={item.id}
          id={item.id}
          name={item.name}
          price={item.price}
          imgSrc={item.imgSrc}
          color={item.color}
          favoriteItems={favoriteItems}
          setFavoriteItems={setFavoriteItems}
          cartItems={cartItems}
          handleBuyClick={() => {
            handleBuyClick()
            setItemId(item.id)
            setCurrentItemName(
              item.id ? productList.filter(elem => elem.id === item.id)[0].name : null,
            )
          }}
        />
      ))}
    </div>
  )
}

ProductList.propTypes = {
  productList: PropTypes.array.isRequired,
  handleBuyClick: PropTypes.func.isRequired,
  setItemId: PropTypes.func.isRequired,
  setCurrentItemName: PropTypes.func.isRequired,
  favoriteItems: PropTypes.array.isRequired,
  setFavoriteItems: PropTypes.func.isRequired,
  cartItems: PropTypes.array.isRequired,
}

export default ProductList
