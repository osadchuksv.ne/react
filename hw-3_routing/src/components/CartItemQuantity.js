import React, {useEffect, useState} from 'react'
import PropTypes from 'prop-types'

function CartItemQuantity({ id, handleCounterClick, cartItems})
{

  const [itemQuantity, setItemQuantity] = useState(0)

  useEffect(()=>{
      const matchItem = cartItems.find(elem => {
        return elem.id === id
      })
      setItemQuantity(matchItem.counter)
  }, [cartItems, id])
  
  return (
  <div className='cart-item__counter'>
    <button className='cart-item__counter-decrement' onClick={handleCounterClick}>-1</button>
    <span className='cart-item__counter-quantity'>{itemQuantity} pcs.</span>
    <button className='cart-item__counter-increment' onClick={handleCounterClick}>+1</button>
  </div>)
}


export default CartItemQuantity
