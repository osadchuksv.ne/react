import React, { useState, useEffect } from 'react'
import CartItemQuantity from './CartItemQuantity'
import PropTypes from 'prop-types'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import Grid from '@mui/material/Grid';
// import Typography from '@mui/material/Typography';
// import DeleteIcon from '@mui/icons-material/Delete';

function CartItem({
  id,
  name,
  price,
  imgSrc,
  color,
  cartItems,
  setCartItems,
  addToCart,
  handleRemoveClick,
  setTotalCartSum,
}) {
  const [itemTotalPrice, setItemTotalPrice] = useState(price)

  const matchItem = cartItems.find(elem => {
    return elem.id === id
  })
  const matchItemIndex = cartItems.indexOf(matchItem)

  useEffect(() => {
    const newPrice = matchItem.counter * price
    setItemTotalPrice(newPrice)
    setTotalCartSum(prevSum => prevSum + newPrice)
  }, [cartItems])

  function handleCounterClick(e) {
    if (e.target.className === 'cart-item__counter-increment') {
      e.preventDefault()
      addToCart(id)
    } else if (e.target.className === 'cart-item__counter-decrement') {
      e.preventDefault()
      cartItems[matchItemIndex].counter - 1 === 0 ? handleRemoveClick() : cartItems[matchItemIndex].counter -= 1
      const updatedCartList = [...cartItems]
      setCartItems(updatedCartList)
      localStorage.setItem('cartItems', JSON.stringify(updatedCartList))
    }
  }

  return (
    <div className="cart-item" key={id}>
      <button
        className="cart-item__remove-from-cart-btn"
        onClick={() => {
          handleRemoveClick()
        }}
      >
        <svg xmlns="http://www.w3.org/2000/svg" height="24" width="21" viewBox="0 0 448 512">
          <path d="M135.2 17.7C140.6 6.8 151.7 0 163.8 0H284.2c12.1 0 23.2 6.8 28.6 17.7L320 32h96c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 96 0 81.7 0 64S14.3 32 32 32h96l7.2-14.3zM32 128H416V448c0 35.3-28.7 64-64 64H96c-35.3 0-64-28.7-64-64V128zm96 64c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16z" />
        </svg>
      </button>
      <div className="product-card__item-photo-container">
        <img className="product-card__item-photo" src={imgSrc} alt={name} />
      </div>
      <div className='cart-item__item-description'>
      <p className="product-card__color">
        Color: <span>{color}</span>
      </p>
      <h2 className="product-card__title">{name}</h2>
      </div>
      <CartItemQuantity
        id={id}
        handleCounterClick={handleCounterClick}
        cartItems={cartItems}
      />
      <div className="product-card__price-buy-container">
        <p className="product-card__price">
          {itemTotalPrice}
          <span> ₴</span>
        </p>
      </div>
    </div>
  )
}

export default CartItem
