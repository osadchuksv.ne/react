import React, { useState, useEffect } from 'react'
import { Routes, Route } from 'react-router-dom'
import './App.scss'

import { Layout } from './components/Layout'

import ActionButton from './components/ActionButton'
import AddToCartModal from './components/AddToCartModal'
import RemoveFromCartModal from './components/RemoveFromCartModal'

import { Homepage } from './pages/Homepage'
import { CartPage } from './pages/CartPage'
import { FavoritesPage } from './pages/FavoritesPage'
import { PageNotFound } from './pages/PageNotFound'

function App() {
  const [productList, setProductList] = useState([])
  const [cartItems, setCartItems] = useState([])
  const [favoriteItems, setFavoriteItems] = useState([])
  const [addToCartModalVisibility, setAddToCartModalVisibility] = useState(false)
  const [removeFromCartModalVisibility, setRemoveFromCartModalVisibility] = useState(false)
  const [itemId, setItemId] = useState(null)
  const [currentItemName, setCurrentItemName] = useState(null)

  useEffect(() => {
    setCartItems(JSON.parse(localStorage.getItem('cartItems')) || [])
    setFavoriteItems(JSON.parse(localStorage.getItem('favoriteItems')) || [])
  }, [])

  useEffect(() => {
    try {
      const fetchData = async () => {
        const data = await fetch('/products.json').then(res => res.json())
        setProductList(data.products)
        console.log(data)
        localStorage.setItem('products', JSON.stringify(data.products))
      }
      fetchData()
    } catch (error) {
      console.error(error)
    }
  }, [])

  const addToCart = id => {
    const matchItem = cartItems.find(elem => {
      return elem.id === id
    })
    let updatedCartList
    if (matchItem) {
      const matchItemIndex = cartItems.indexOf(matchItem)
      cartItems[matchItemIndex].counter += 1
      updatedCartList = [...cartItems]
    } else {
      const newCartItem = { id: id, counter: 1 }
      updatedCartList = [...cartItems, newCartItem]
    }

    setCartItems(updatedCartList)

    localStorage.setItem('cartItems', JSON.stringify(updatedCartList))
  }

  const removeFromCart = id => {
    const matchItem = cartItems.find(elem => {
      return elem.id === id
    })
    const matchItemIndex = cartItems.indexOf(matchItem)
    cartItems.splice(matchItemIndex, 1)
    const updatedCartList = [...cartItems]

    setCartItems(updatedCartList)

    localStorage.setItem('cartItems', JSON.stringify(updatedCartList))
  }

  const addToCartModalActionBtns = (
    <div className="modal__actions">
      <ActionButton
        name="Add to cart"
        className="add-to-cart-btn modal-btn"
        handleClick={() => {
          addToCart(itemId)
          setAddToCartModalVisibility(false)
          setItemId(null)
        }}
      />
      <ActionButton
        name="Cancel"
        className="cancel-btn modal-btn "
        handleClick={() => {
          setAddToCartModalVisibility(false)
          setItemId(null)
        }}
      />
    </div>
  )

  const removeFromCartModalActionBtns = (
    <div className="modal__actions">
      <ActionButton
        name="Remove from cart"
        className="modal-btn ok-btn"
        handleClick={() => {
          removeFromCart(itemId)
          setRemoveFromCartModalVisibility(false)
          setItemId(null)
        }}
      />
      <ActionButton
        name="Cancel"
        className="modal-btn cancel-btn"
        handleClick={() => {
          setRemoveFromCartModalVisibility(false)
          setItemId(null)
        }}
      />
    </div>
  )

  return (
    <>
      {
        <Routes>
          <Route
            path="/"
            element={
              <Layout
                cartItems={cartItems}
                favoriteItems={favoriteItems}
              />
            }
          >
            <Route
              index
              path="/"
              element={
                <Homepage
                  productList={productList}
                  setItemId={setItemId}
                  setAddToCartModalVisibility={setAddToCartModalVisibility}
                  setCurrentItemName={setCurrentItemName}
                  favoriteItems={favoriteItems}
                  setFavoriteItems={setFavoriteItems}
                  cartItems={cartItems}
                  // setCartItems={setCartItems}
                />
              }
            />
            <Route
              path="cart"
              element={
                <CartPage
                  productList={productList}
                  cartItems={cartItems}
                  setCartItems={setCartItems}
                  addToCart={addToCart}
                  setItemId={setItemId}
                  setRemoveFromCartModalVisibility={setRemoveFromCartModalVisibility}
                  setCurrentItemName={setCurrentItemName}
                />
              }
            />
            <Route
              path="favorites"
              element={
                <FavoritesPage
                  productList={productList}
                  setItemId={setItemId}
                  setAddToCartModalVisibility={setAddToCartModalVisibility}
                  setCurrentItemName={setCurrentItemName}
                  favoriteItems={favoriteItems}
                  setFavoriteItems={setFavoriteItems}
                  cartItems={cartItems}
                  setCartItems={setCartItems}
                />
              }
            />
            <Route path="*" element={<PageNotFound />} />
          </Route>
        </Routes>
      }

      {addToCartModalVisibility && (
        <AddToCartModal
          header="Adding to cart confirmation"
          closeButton="true"
          text={`By clicking "Add to cart" button you will add ${currentItemName} to the cart. To buy chosen item you should go to the cart page and confirm purchase`}
          actions={addToCartModalActionBtns}
          active={addToCartModalVisibility}
          setActive={setAddToCartModalVisibility}
          itemId={itemId}
        />
      )}

      {removeFromCartModalVisibility && (
        <RemoveFromCartModal
          header='Remove from cart?'
          closeButton="true"
          text={`Do you really want do remove ${currentItemName} from cart?`}
          actions={removeFromCartModalActionBtns}
          active={removeFromCartModalVisibility}
          setActive={setRemoveFromCartModalVisibility}
        />
      )}
    </>
  )
}

export default App
