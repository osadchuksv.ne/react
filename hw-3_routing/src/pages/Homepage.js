import React from 'react'
import ProductList from '../components/ProductsList'

const Homepage = ({
  productList,
  setItemId,
  setAddToCartModalVisibility,
  setCurrentItemName,
  favoriteItems,
  setFavoriteItems,
  cartItems
}) => {
  return (
    <section className="container">
        <h1 className="page-title">Smartphones in stock</h1>
        <ProductList
          productList={productList}
          setItemId={setItemId}
          handleBuyClick={() => {
            setAddToCartModalVisibility(true)
          }}
          setCurrentItemName={setCurrentItemName}
          favoriteItems={favoriteItems}
          setFavoriteItems={setFavoriteItems}
          cartItems={cartItems}
        />
    </section>
  )
}

export { Homepage }
