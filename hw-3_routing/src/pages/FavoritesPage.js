import React from 'react'
import Card from '../components/Card'

const FavoritesPage = ({
  productList,
  handleBuyClick,
  handleFavIconClick,
  setItemId,
  setCurrentItemName,
  favoriteItems,
  setFavoriteItems,
  cartItems,
  setCartItems,
}) => {
  return (
    <section className="favorites-page  container">
      {favoriteItems.length > 0 ? <h1 className='page-title'>Favorite items</h1> : <h1 className='page-title'>No items have been added to favorites yet</h1>}
        <div className="cards-wrapper">
          {productList
            .filter(elem => favoriteItems.some(item => item === elem.id))
            .map(item => (
              <Card
                key={item.id}
                id={item.id}
                name={item.name}
                price={item.price}
                imgSrc={item.imgSrc}
                color={item.color}
                favoriteItems={favoriteItems}
                setFavoriteItems={setFavoriteItems}
                cartItems={cartItems}
                setCartItems={setCartItems}
                handleBuyClick={() => {
                  handleBuyClick()
                  setItemId(item.id)
                  setCurrentItemName(
                    item.id ? productList.filter(elem => elem.id === item.id)[0].name : null,
                  )
                }}
                handleFavIconClick={() => {
                  handleFavIconClick(item.id)
                }}
              />
            ))}
        </div>
    </section>
  )
}

export { FavoritesPage }
