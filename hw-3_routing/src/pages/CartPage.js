import React, { useEffect, useState } from 'react'
import CartItem from '../components/CartItem'
import Button from '../components/Button'

const CartPage = ({
  productList,
  cartItems,
  setCartItems,
  addToCart,
  setItemId,
  setRemoveFromCartModalVisibility,
  setCurrentItemName,
}) => {
  useEffect(() => {
    const filteredProductList = productList.filter(elem => {
      return cartItems.find(item => {
        return item.id === elem.id
      })
    })
    let priceSum = 0
    filteredProductList.forEach(item => {
      const counter = cartItems.find(elem => {
        return elem.id === item.id
      }).counter
      const elemPrice = item.price * counter
      priceSum += elemPrice
    })
    setTotalCartSum(priceSum)
  }, [cartItems])

  const [totalCartSum, setTotalCartSum] = useState(0)

  return (
    <section className="cart-page container">
      {cartItems.length > 0 ? (
        <h1 className="page-title">Products in cart</h1>
      ) : (
        <h1 className="page-title">No items have been added to cart yet</h1>
      )}
      <div className="cart-page__content">
        {productList
          .filter(elem => cartItems.some(item => item.id === elem.id))
          .map(item => (
            <CartItem
              key={item.id}
              id={item.id}
              name={item.name}
              price={item.price}
              imgSrc={item.imgSrc}
              color={item.color}
              cartItems={cartItems}
              setCartItems={setCartItems}
              addToCart={addToCart}
              setItemId={setItemId}
              setRemoveFromCartModalVisibility={setRemoveFromCartModalVisibility}
              handleRemoveClick={() => {
                setRemoveFromCartModalVisibility(true)
                setItemId(item.id)
                setCurrentItemName(productList.filter(elem => elem.id === item.id)[0].name)
              }}
              setTotalCartSum={setTotalCartSum}
            />
          ))}
        {cartItems.length > 0 && (
          <div className="cart-page__total-next-step-div">
            <p className="cart-page__total-sum">
              Total: <span className="cart-page__total-sum_number">{totalCartSum}</span>
              <span> ₴</span>
            </p>
            <Button
              background="green"
              name="To next step"
              onClick={e => {
                e.preventDefault()
              }}
              className="cart-page__next-step-btn"
            />
          </div>
        )}
      </div>
    </section>
  )
}

export { CartPage }
