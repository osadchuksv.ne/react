import React from 'react'
import { Routes, Route } from 'react-router-dom'
import { Layout } from '../components/Layout'

import { Homepage } from '../pages/Homepage'
import { CartPage } from '../pages/CartPage'
import { FavoritesPage } from '../pages/FavoritesPage'
import { PageNotFound } from '../pages/PageNotFound'

const AppRoutes = () => {

    return (
        <>
          {
            <Routes>
              <Route
                path="/"
                element={
                  <Layout/>
                }
              >
                <Route
                  index
                  path="/"
                  element={
                    <Homepage/>
                  }
                />
                <Route
                  path="cart"
                  element={
                    <CartPage/>
                  }
                />
                <Route
                  path="favorites"
                  element={
                    <FavoritesPage/>
                  }
                />
                <Route path="*" element={<PageNotFound />} />
              </Route>
            </Routes>
          }
        </>
      )
  }

export {AppRoutes}