import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  setSelectedItemId,
  setSelectedItemName,
  addToCart,
  cartItemDecrement,
  openModal,
} from '../redux/slices/productsSlice'

import CartItemQuantity from './CartItemQuantity'
import RemoveFromCartModal from './RemoveFromCartModal'
import PropTypes from 'prop-types'

function CartItem({
  id,
  name,
  price,
  imgSrc,
  color,
  setTotalCartSum,
}) {
  const [itemTotalPrice, setItemTotalPrice] = useState(price)

  const dispatch = useDispatch()

  const cartItems = useSelector(state => state.products.cartItems)
  const isModalOpen = useSelector(state => state.products.isModalOpen)
  const selectedItemId = useSelector(state => state.products.selectedItemId)
  const selectedItemName = useSelector(state => state.products.selectedItemName)

  const matchItem = cartItems.find(elem => {
    return elem.id === id
  })

  useEffect(() => {
    const newPrice = matchItem.counter * price
    setItemTotalPrice(newPrice)
    setTotalCartSum(prevSum => prevSum + newPrice)
  }, [cartItems])

  function handleRemoveBtnClick() {
    dispatch(openModal())
    dispatch(setSelectedItemId(id))
    dispatch(setSelectedItemName(name))
  }

  function handleCounterClick(e) {
    if (e.target.className === 'cart-item__counter-increment') {
      e.preventDefault()
      dispatch(addToCart(id))
    } else if (e.target.className === 'cart-item__counter-decrement') {
      e.preventDefault()
      dispatch(setSelectedItemId(id))
      dispatch(setSelectedItemName(name))
      dispatch(cartItemDecrement(id))
    }
  }

  return (
    <div className="cart-item" key={id}>
      <button
        className="cart-item__remove-from-cart-btn"
        onClick={() => {
          handleRemoveBtnClick(id, name)
        }}
      >
        <svg xmlns="http://www.w3.org/2000/svg" height="24" width="21" viewBox="0 0 448 512">
          <path d="M135.2 17.7C140.6 6.8 151.7 0 163.8 0H284.2c12.1 0 23.2 6.8 28.6 17.7L320 32h96c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 96 0 81.7 0 64S14.3 32 32 32h96l7.2-14.3zM32 128H416V448c0 35.3-28.7 64-64 64H96c-35.3 0-64-28.7-64-64V128zm96 64c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16z" />
        </svg>
      </button>
      <div className="product-card__item-photo-container">
        <img className="product-card__item-photo" src={imgSrc} alt={name} />
      </div>
      <div className="cart-item__item-description">
        <p className="product-card__color">
          Color: <span>{color}</span>
        </p>
        <h2 className="product-card__title">{name}</h2>
      </div>
      <CartItemQuantity
        id={id}
        handleCounterClick={(e) => {
          handleCounterClick(e, id, name)
        }}
      />
      <div className="product-card__price-buy-container">
        <p className="product-card__price">
          {itemTotalPrice}
          <span> ₴</span>
        </p>
      </div>
      {isModalOpen && (
        <RemoveFromCartModal
          header="Remove from cart?"
          closeButton="true"
          text={`Do you really want do remove ${selectedItemName} from cart?`}
        />
      )}
    </div>
  )
}

CartItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  imgSrc: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  setTotalCartSum: PropTypes.func.isRequired
}

export default CartItem
