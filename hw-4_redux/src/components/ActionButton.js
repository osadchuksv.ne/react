import React from 'react'
import PropTypes from 'prop-types'

function ActionButton({ name, className, handleClick }) {
  return (
    <>
      <button className={className} onClick={handleClick}>
        {name}
      </button>
    </>
  )
}

ActionButton.propTypes = {
  name: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired,
}

export default ActionButton
