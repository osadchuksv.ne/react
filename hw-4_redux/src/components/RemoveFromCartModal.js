import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  setSelectedItemId,
  setSelectedItemName,
  removeFromCart,
  closeModal,
} from '../redux/slices/productsSlice'
import ActionButton from './ActionButton'
import PropTypes from 'prop-types'

function RemoveFromCartModal({ header, closeButton, text }) {
  const dispatch = useDispatch()

  const selectedItemId = useSelector(state => state.products.selectedItemId)
  const selectedItemName = useSelector(state => state.products.selectedItemName)

  const handleRemoveClick = selectedItemId => {
    dispatch(removeFromCart(selectedItemId))
    dispatch(closeModal())
    dispatch(setSelectedItemId(null))
    dispatch(setSelectedItemName(null));
  }

  const handleCloseModal = ()=>{
    dispatch(setSelectedItemId(null));
    dispatch(setSelectedItemName(null));
    dispatch(closeModal())
  }

  const removeFromCartModalActionBtns = (
    <div className="modal__actions">
      <ActionButton
        name="Remove from cart"
        className="modal-btn ok-btn"
        handleClick={() => {
          handleRemoveClick()
        }}
      />
      <ActionButton
        name="Cancel"
        className="modal-btn cancel-btn"
        handleClick={() => {
          handleCloseModal()
        }}
      />
    </div>
  )

  return (
    <div
      className="modal-backdrop"
      onClick={() => {
        handleCloseModal()
      }}
    >
      <div
        className="modal delete-modal"
        onClick={event => {
          event.stopPropagation()
        }}
      >
        <h1 className="modal__header delete-modal__header">{header}</h1>
        {{ closeButton } && (
          <button
            className="modal__close-btn"
            onClick={() => {
              handleCloseModal()
            }}
          ></button>
        )}
        <p className="modal__body">{text}</p>
        {removeFromCartModalActionBtns}
      </div>
    </div>
  )
}

RemoveFromCartModal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.string,
  text: PropTypes.string.isRequired,
}

export default RemoveFromCartModal
