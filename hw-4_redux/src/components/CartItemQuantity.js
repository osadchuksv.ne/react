import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'

function CartItemQuantity({ id, handleCounterClick}) {
  const [itemQuantity, setItemQuantity] = useState(0)

  const cartItems = useSelector(state => state.products.cartItems)

  useEffect(() => {
    const matchItem = cartItems.find(elem => {
      return elem.id === id
    })
    setItemQuantity(matchItem.counter)
  }, [cartItems, id])

  return (
    <div className="cart-item__counter">
      <button className="cart-item__counter-decrement" onClick={handleCounterClick}>
        -1
      </button>
      <span className="cart-item__counter-quantity">{itemQuantity} pcs.</span>
      <button className="cart-item__counter-increment" onClick={handleCounterClick}>
        +1
      </button>
    </div>
  )
}

CartItemQuantity.propTypes = {
  id: PropTypes.number.isRequired,
  handleCounterClick: PropTypes.func.isRequired
}

export default CartItemQuantity
