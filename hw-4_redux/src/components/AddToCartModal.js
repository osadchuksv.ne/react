import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {setSelectedItemId, setSelectedItemName, addToCart, closeModal} from '../redux/slices/productsSlice'
import ActionButton from './ActionButton'
import PropTypes from 'prop-types'

function AddToCartModal({ header, closeButton, text}) {

  const dispatch = useDispatch();
  const selectedItemId = useSelector((state) => state.products.selectedItemId);



  const handleAddToCartClick = ()=>{
    dispatch(addToCart(selectedItemId));
    dispatch(setSelectedItemId(null));
    dispatch(setSelectedItemName(null));
    dispatch(closeModal())
  }

  const handleCloseModal = ()=>{
    dispatch(setSelectedItemId(null));
    dispatch(setSelectedItemName(null));
    dispatch(closeModal())
  }


  const addToCartModalActionBtns = (
    <div className="modal__actions">
      <ActionButton
        name="Add to cart"
        className="add-to-cart-btn modal-btn"
        handleClick={() => {
          handleAddToCartClick(selectedItemId)
        }}
      />
      <ActionButton
        name="Cancel"
        className="cancel-btn modal-btn "
        handleClick={() => {
          handleCloseModal()
        }}
      />
    </div>
  )

  return (
    <div
      className="modal-backdrop"
      onClick={() => {
        handleCloseModal()
      }}
    >
      <div
        className="modal add-to-cart-modal"
        onClick={event => {
          event.stopPropagation()
        }}
      >
        <h1 className="modal__header add-to-cart-modal__header">{header}</h1>
        {{ closeButton } && (
          <button
            className="modal__close-btn"
            onClick={() => {
              handleCloseModal()
            }}
          ></button>
        )}
        <p className="modal__body add-to-cart-modal__body">{text}</p>
        {addToCartModalActionBtns}
      </div>
    </div>
  )
}

AddToCartModal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.string,
  text: PropTypes.string.isRequired,
}

AddToCartModal.defaultProps = {
  header: 'Adding to cart confirmation',
  closeButton: 'true',
}

export default AddToCartModal
