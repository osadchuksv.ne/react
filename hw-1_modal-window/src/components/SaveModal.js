import React from 'react'


function SaveModal({ header, closeButton, text, actions, setActive }) {
  return (
    <div
      className="modal-backdrop"
      onClick={() => {
        setActive(false)
      }}
    >
      <div
        className="modal save-modal"
        onClick={event => {
          event.stopPropagation()
        }}
      >
        <h1 className="modal__header save-modal__header">{header}</h1>
        {{ closeButton } && (
          <button
            className="modal__close-btn"
            onClick={() => {
              setActive(false)
            }}
          >
          </button>
        )}
        <p className="modal__body save-modal__body">{text}</p>
        {actions}
      </div>
    </div>
  )
}

export default SaveModal
