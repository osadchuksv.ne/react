import React from 'react'

function ActionButton({ name, className, handleClick }) {
  return (
    <div>
      <button className={className} onClick={handleClick}>
        {name}
      </button>
    </div>
  )
}

export default ActionButton
