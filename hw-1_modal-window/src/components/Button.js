import React from 'react'

function Button({ background, name, onClick }) {
  return (
    <div>
      <button
        className="open-modal-btn btn"
        style={{ backgroundColor: background }}
        onClick={onClick}
      >
        {name}
      </button>
    </div>
  )
}

export default Button
