import React, { useState } from 'react'
import './App.scss'
import Button from './components/Button'
import DeleteModal from './components/DeleteModal'
import SaveModal from './components/SaveModal'
import ActionButton from './components/ActionButton'


function App() {
  const [deleteModalVisibility, setDeleteModalVisibility] = useState(false)
  const [saveModalVisibility, setSaveModalVisibility] = useState(false)

  const deleteModalActionBtns = (
    <div className="modal__actions">
      <ActionButton
        name="Ok"
        className="modal-btn ok-btn"
        handleClick={() => {
          setDeleteModalVisibility(false)
        }}
      />
      <ActionButton
        name="Cancel"
        className="modal-btn cancel-btn"
        handleClick={() => {
          setDeleteModalVisibility(false)
        }}
      />
    </div>
  )

  const saveModalActionBtns = (
    <div className="modal__actions">
      <ActionButton
        name="Save"
        className="modal-btn save-btn"
        handleClick={() => {
          setSaveModalVisibility(false)
        }}
      />
      <ActionButton
        name="Discard"
        className="modal-btn discard-btn"
        handleClick={() => {
          setSaveModalVisibility(false)
        }}
      />
    </div>
  )

  return (
    <div className="App">
      <div className="open-btn-wrapper">
        <Button
          background={'#d72f2f'}
          name="Open first modal"
          onClick={() => setDeleteModalVisibility(true)}
        />
        <Button
          background={'#41c141'}
          name="Open second modal"
          onClick={() => setSaveModalVisibility(true)}
        />
      </div>
      {deleteModalVisibility && (
        <DeleteModal
          header="Do you want to delete this file?"
          closeButton="true"
          text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
          actions={deleteModalActionBtns}
          active={deleteModalVisibility}
          setActive={setDeleteModalVisibility}
        />
      )}
      {saveModalVisibility && (
        <SaveModal
          header="Save changes?"
          closeButton="true"
          text="Clicking the save button will save you changes in this file"
          actions={saveModalActionBtns}
          active={saveModalVisibility}
          setActive={setSaveModalVisibility}
        />
      )}
    </div>
  )
}

export default App
