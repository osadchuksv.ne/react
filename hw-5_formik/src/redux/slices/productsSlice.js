import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

export const fetchProducts = createAsyncThunk('products/fetchData', async () => {
  try {
    const res = await fetch('/products.json')
    return await res.json()
  } catch (error) {
    console.error('Error while trying to get products', error)
    throw error
  }
})

const initialValue = {
  productsList: JSON.parse(localStorage.getItem('products')) || [],
  cartItems: JSON.parse(localStorage.getItem('cartItems')) || [],
  favoriteItems: JSON.parse(localStorage.getItem('favorites')) || [],
  isModalOpen: false,
  selectedItemId: null,
  selectedItemName: null,
  status: 'idle',
}

const productsSlice = createSlice({
  name: 'products',
  initialState: initialValue,
  reducers: {
    setSelectedItemId(state, action) {
      state.selectedItemId = action.payload
    },

    setSelectedItemName(state, action) {
      state.selectedItemName = action.payload
    },

    addToCart(state, action) {
      const matchItem = state.cartItems.find(elem => {
        return elem.id === action.payload
      })
      if (matchItem) {
        const matchItemIndex = state.cartItems.indexOf(matchItem)
        state.cartItems[matchItemIndex].counter += 1
      } else {
        const newCartItem = { id: action.payload, counter: 1 }
        state.cartItems.push(newCartItem)
      }
      localStorage.setItem('cartItems', JSON.stringify(state.cartItems))
    },

    cartItemDecrement(state, action) {
      const matchItem = state.cartItems.find(elem => {
        return elem.id === action.payload
      })
      const matchItemIndex = state.cartItems.indexOf(matchItem)

      state.cartItems[matchItemIndex].counter - 1 === 0
        ? (state.isModalOpen = true)
        : (state.cartItems[matchItemIndex].counter -= 1)
    },

    removeFromCart(state, action) {
      const matchItem = state.cartItems.find(elem => {
        return elem.id === action.payload
      })
      const matchItemIndex = state.cartItems.indexOf(matchItem)
      state.cartItems.splice(matchItemIndex, 1)

      localStorage.setItem('cartItems', JSON.stringify(state.cartItems))
    },

    clearCart(state) {
      state.cartItems = []
      localStorage.removeItem('cartItems')
    },

    toggleFavorite(state, action) {
      const id = action.payload
      state.favoriteItems = state.favoriteItems.includes(id)
        ? state.favoriteItems.filter(elem => elem !== id)
        : [...state.favoriteItems, id]
      localStorage.setItem('favorites', JSON.stringify(state.favoriteItems))
    },

    openModal(state) {
      state.isModalOpen = true
    },

    closeModal(state) {
      state.isModalOpen = false
    },
  },

  extraReducers: builder => {
    builder
      .addCase(fetchProducts.pending, state => {
        state.status = 'loading'
      })
      .addCase(fetchProducts.fulfilled, (state, action) => {
        state.status = 'resolved'
        state.productsList = action.payload.products
        localStorage.setItem('products', JSON.stringify(state.productsList))
      })
      .addCase(fetchProducts.rejected, (state, action) => {
        state.status = 'rejected'
        state.error = action.error.message
      })
  },
})

export const {
  setProductsList,
  setSelectedItemId,
  setSelectedItemName,
  addToCart,
  cartItemDecrement,
  removeFromCart,
  clearCart,
  toggleFavorite,
  openModal,
  closeModal,
} = productsSlice.actions
export default productsSlice.reducer
