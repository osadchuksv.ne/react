import React from 'react'
import ProductList from '../components/ProductsList'

const Homepage = () => {
  return (
    <section className="container">
      <h1 className="page-title">Smartphones in stock</h1>
      <ProductList />
    </section>
  )
}

export { Homepage }
