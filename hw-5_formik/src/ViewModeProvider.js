// import { useContext } from 'react'
import Context from './context'
import { useEffect, useState } from 'react'

const ViewModeProvider = ({ children }) => {
  const [currentMode, setCurrentMode] = useState('')

  useEffect(() => {
    setCurrentMode(localStorage.getItem('viewMode') || 'card')

    // setCurrentMode(localStorage.getItem('viewMode') || 'card')
  }, [])

  // () => {
  //   const mode = localStorage.getItem('viewMode')
  //   return mode ? mode : 'card'

  //   useEffect(() => {
  //     setCurrentMode(localStorage.getItem('viewMode') || 'card')
  //   }, [])

  const handleToggleClick = () => {
    // const newMode = currentMode === 'card' ? setCurrentMode('table') : setCurrentMode('card')
    setCurrentMode(prev => {
      const newMode = prev === 'table' ? 'card' : 'table'
      console.log(newMode)
      localStorage.setItem('viewMode', newMode)
      return newMode
    })
  }

  // useEffect(() => {
  //   const mode = localStorage.getItem('viewMode')
  //   if (mode) {
  //     setCurrentMode(mode)
  //   }
  //   // setCurrentMode(localStorage.getItem('viewMode') || 'card')
  // }, [])
  console.log(currentMode)

  return (
    <Context.Provider value={{ currentMode, handleToggleClick }}>{children}</Context.Provider>
  )
}

export default ViewModeProvider
