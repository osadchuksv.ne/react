import { number, object, string } from 'yup'

const validationSchema = object({
  'First name': string('First name must be a string')
    .min(2, 'Name is too short')
    .max(50, 'Name is too long')
    .matches(
      /^([A-Za-z\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff\s]*)$/gi,
      'Name can only contain Latin letters.',
    )
    .required('First name is required'),
  'Last name': string('Last name must be a string')
    .min(2, 'Last name is too short')
    .max(50, 'Last name is too long')
    .matches(
      /^([A-Za-z\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff\s]*)$/gi,
      'Last name can only contain Latin letters.',
    )
    .required('Last name is required'),
  Age: number('Age must be a number')
    .min(16, 'Age must be not less than 16')
    .max(120, 'Age must be not more than 120')
    .integer('Age must be an integer')
    .required('Age is required'),
  'Delivery address': string('Address must be a string').required(
    'Delivery address is required',
  ),
  'Phone number': string()
    .matches(
      /^[0-9]+$/,
      'Phone number must contain only digits. Fill in the number in formatt xx(xxx)xxxxxxx',
    )
    .min(10, 'Phone number must be at least 10 digits')
    .max(12, 'Phone number must have max 12 digits')
    .required('Phone number is required'),
})

export default validationSchema
