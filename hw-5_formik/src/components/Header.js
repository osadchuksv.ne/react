import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { useContext } from 'react'
import Context from '../context'

function Header() {
  const favoriteItems = useSelector(state => state.products.favoriteItems)
  const cartItems = useSelector(state => state.products.cartItems)

  // const context = useContext(Context)
  // console.log(context.currentMode)

  return (
    <div className="header">
      {/* <button onClick={context.handleToggleClick}>change view mode</button> */}
      <nav className="header__nav-menu container">
        <Link to="/">
          <div className="header__nav-menu_logo"></div>
        </Link>
        <div className="header__nav-menu_icons-container">
          <div className="header__nav-menu_favorites">
            <Link to="/favorites">
              <svg
                className="header__nav-menu_favorites-icon"
                xmlns="http://www.w3.org/2000/svg"
                width="56"
                height="56"
                fill="none"
                viewBox="0 0 28 28"
              >
                <g clipPath="url(#a)">
                  <path
                    fill={favoriteItems?.length > 0 ? '#FFD90D' : '#ffff'}
                    fillRule="evenodd"
                    stroke="black"
                    d="M14.001.64a.968.968 0 0 0-.86.528L9.655 8a1.724 1.724 0 0 1-1.265.919L.815 10.122a.968.968 0 0 0-.532 1.64l5.42 5.426a1.723 1.723 0 0 1 .483 1.486L4.99 26.25a.968.968 0 0 0 1.394 1.013l6.837-3.478a1.724 1.724 0 0 1 1.562 0l6.837 3.478a.968.968 0 0 0 1.394-1.013l-1.195-7.576a1.721 1.721 0 0 1 .483-1.486l5.42-5.426a.968.968 0 0 0-.532-1.64l-7.575-1.204a1.724 1.724 0 0 1-1.265-.919l-3.486-6.83A.968.968 0 0 0 14 .64Z"
                    clipRule="evenodd"
                  />
                </g>
                <defs>
                  <clipPath id="a">
                    <path fill="#fff" d="M0 0h28v28H0z" />
                  </clipPath>
                </defs>
              </svg>
            </Link>
            {favoriteItems?.length > 0 && (
              <span className="header__nav-menu_favorites-counter">
                {favoriteItems?.length}
              </span>
            )}
          </div>
          <div className="header__nav-menu_shopping-cart">
            <Link to="/cart">
              <svg
                stroke="black"
                fill={cartItems?.length > 0 ? '#FFD90D' : '#ffff'}
                strokeWidth="15"
                viewBox="0 0 576 512"
                className="header__nav-menu_shopping-cart-icon"
                height="1em"
                width="1em"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M528.12 301.319l47.273-208C578.806 78.301 567.391 64 551.99 64H159.208l-9.166-44.81C147.758 8.021 137.93 0 126.529 0H24C10.745 0 0 10.745 0 24v16c0 13.255 10.745 24 24 24h69.883l70.248 343.435C147.325 417.1 136 435.222 136 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-15.674-6.447-29.835-16.824-40h209.647C430.447 426.165 424 440.326 424 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-22.172-12.888-41.332-31.579-50.405l5.517-24.276c3.413-15.018-8.002-29.319-23.403-29.319H218.117l-6.545-32h293.145c11.206 0 20.92-7.754 23.403-18.681z"></path>
              </svg>
            </Link>
            {cartItems?.length > 0 && (
              <span className="header__nav-menu_shopping-cart-counter">
                {cartItems?.length}
              </span>
            )}
          </div>
        </div>
      </nav>
    </div>
  )
}

export default Header
